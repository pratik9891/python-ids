import math


# Used for liner scaling
DURATION_MAX = 58329
WRONG_FRAGMENT_MAX = 3
URGENT_MAX = 14
HOT_MAX = 101
NUM_FAILED_LOGINS_MAX = 5
NUM_COMPROMISED_MAX = 9
SU_ATTEMPTED_MAX = 2
NUM_ROOT_MAX = 7468
NUM_FILE_CREATIONS_MAX = 100
NUM_SHELLS_MAX = 5
NUM_ACCESS_FILES_MAX = 9
COUNT_MAX = 511
SRV_COUNT_MAX = 511
DST_HOST_COUNT_MAX = 255
DST_HOST_SRV_COUNT_MAX = 255

# Maping strings to ints using dictionaries and liner scaling

pFlags = {"SF":0,"S2":1,"S1":2,"S3":3,"OTH":4,"REJ":5,"RSTO":6,"S0":7,"RSTR":8,"RSTOS0":9,"SH":10}
#pLabel = {"normal.":0,"probing.":1,"DoS.":2,"U2R.":3,"R2L.":4}
pLabel = {"None":None,
          "normal.":0, #normal
          "ipsweep.":1,"nmap.":1,"portsweep.":1,"satan.":1, #probing
          "back.":2,"land.":2,"neptune.":2,"pod.":2,"smurf.":2,"teardrop.":2, #DoS
          "buffer_overflow.":3,"loadmodule.":3,"perl.":3,"rootkit.":3, #U2R
          "ftp_write.":4,"guess_passwd.":4,"imap.":4,"multihop.":4,"phf.":4,"spy.":4,"warezclient.":4,"warezmaster.":4} #R2L
pProtocol = {"tcp":0,"udp":1,"icmp":2}
pService = {"http":0,"smtp":1,"domain_u":2,"auth":3,"finger":4,"telnet":5,"eco_i":6,"ftp":7,"ntp_u":8,\
            "ecr_i":9,"other":10,"urp_i":11,"private":12,"pop_3":13,"ftp_data":14,"netstat":15,\
            "daytime":16,"ssh":17,"echo":18,"time":19,"name":20,"whois":21,"domain":22,"mtp":23,"gopher":24,\
            "remote_job":25,"rje":26,"ctf":27,"supdup":28,"link":29,"systat":30,"discard":31,"X11":32,\
            "shell":33,"login":34,"imap4":35,"nntp":36,"uucp":37,"pm_dump":38,"IRC":39,"Z39_50":40,\
            "netbios_dgm":41,"ldap":42,"sunrpc":43,"courier":44,"exec":45,"bgp":46,"csnet_ns":47,"http_443":48,\
            "klogin":49,"printer":50,"netbios_ssn":51,"pop_2":52,"nnsp":53,"efs":54,"hostnames":55,"uucp_path":56,\
            "sql_net":57,"vmnet":58,"iso_tsap":59,"netbios_ns":60,"kshell":61,"urh_i":62,"http_2784":63,\
            "harvest":64,"aol":65,"tftp_u":66,"http_8001":67,"tim_i":68,"red_i":69,"icmp":70}
rLabel = { 0:"normal", 1:"probing",2:"DoS",3:"U2R",4:"R2L"} #for writing text instead of numbers in the result


AMOUNT_OF_FLAGS = 11        
AMOUNT_OF_LABELS = 5
AMOUNT_OF_PROTOCOLS = 3
AMOUNT_OF_SERVICES = 70

class networkTraffic(object):
    def __init__(self,duration,protocol_type,service,flag,src_bytes,dst_bytes,land,wrong_fragment,urgent,hot,num_failed_logins,logged_in,num_compromised,root_shell,su_attempted,num_root,num_file_creations,num_shells,num_access_files,num_outbound_cmds,is_host_login,is_guest_login,count,srv_count,serror_rate,srv_serror_rate,rerror_rate,srv_rerror_rate,same_srv_rate,diff_srv_rate,srv_diff_host_rate,dst_host_count,dst_host_srv_count,dst_host_same_srv_rate,dst_host_diff_srv_rate,dst_host_same_src_port_rate,dst_host_srv_diff_host_rate,dst_host_serror_rate,dst_host_srv_serror_rate,dst_host_rerror_rate,dst_host_srv_rerror_rate,label):
        self.duration = int(duration)/DURATION_MAX
        self.protocol_type = pProtocol[protocol_type]/(AMOUNT_OF_PROTOCOLS-1)
        self.service=pService[service]/(AMOUNT_OF_SERVICES-1)
        self.flag=pFlags[flag]/(AMOUNT_OF_FLAGS-1)
        if int(src_bytes)!=0:
            self.src_bytes=math.log(int(src_bytes))  #logarithmically scaled
        else:
            self.src_bytes = 0
        if int(dst_bytes)!=0:
            self.dst_bytes=math.log(int(dst_bytes))  #logarithmically scaled
        else:
            self.dst_bytes = 0
        self.land=int(land)
        self.wrong_fragment=int(wrong_fragment)/WRONG_FRAGMENT_MAX
        self.urgent=int(urgent)/URGENT_MAX
        self.hot=int(hot)/HOT_MAX
        self.num_failed_logins=int(num_failed_logins)/NUM_FAILED_LOGINS_MAX
        self.logged_in=int(logged_in)
        self.num_compromised=int(num_compromised)/NUM_COMPROMISED_MAX
        self.root_shell=int(root_shell)
        self.su_attempted=int(su_attempted)/SU_ATTEMPTED_MAX
        self.num_root=int(num_root)/NUM_ROOT_MAX
        self.num_file_creations=int(num_file_creations)/NUM_FILE_CREATIONS_MAX
        self.num_shells=int(num_shells)/NUM_SHELLS_MAX
        self.num_access_files=int(num_access_files)/NUM_ACCESS_FILES_MAX
        self.num_outbound_cmds=int(num_outbound_cmds)
        self.is_host_login=int(is_host_login)
        self.is_guest_login=int(is_guest_login)
        self.count=int(count)/COUNT_MAX
        self.srv_count=int(srv_count)/SRV_COUNT_MAX
        self.serror_rate=float(serror_rate)
        self.srv_serror_rate=float(srv_serror_rate)
        self.rerror_rate=float(rerror_rate)
        self.srv_rerror_rate=float(srv_rerror_rate)
        self.same_srv_rate=float(same_srv_rate)
        self.diff_srv_rate=float(diff_srv_rate)
        self.srv_diff_host_rate=float(srv_diff_host_rate)
        self.dst_host_count=int(dst_host_count)/DST_HOST_COUNT_MAX
        self.dst_host_srv_count=int(dst_host_srv_count)/DST_HOST_SRV_COUNT_MAX
        self.dst_host_same_srv_rate=float(dst_host_same_srv_rate)
        self.dst_host_diff_srv_rate=float(dst_host_diff_srv_rate)
        self.dst_host_same_src_port_rate=float(dst_host_same_src_port_rate)
        self.dst_host_srv_diff_host_rate=float(dst_host_srv_diff_host_rate)
        self.dst_host_serror_rate=float(dst_host_serror_rate)
        self.dst_host_srv_serror_rate=float(dst_host_srv_serror_rate)
        self.dst_host_rerror_rate=float(dst_host_rerror_rate)
        self.dst_host_srv_rerror_rate=float(dst_host_srv_rerror_rate)
        self.label=pLabel[label]

def knn(data, testData, neighbours):
    dataSize = len(data)
    testSize = len(testData)
    confusionMatrix = [] #Amount of different attack labels
    for i in range(5):
        confusionMatrix.append([0,0,0,0,0])
        
    distance = [] #distance and label from each point in data to the point being tested
    label = []
    
    knnMinDistances = [] #K-nearest distances and associated labels to the point being tested
    knnLabels = [] 

    knnGuesses = [] #Amount of guesses of each label (max will be the final guess)
    fo = open("result","wb")
    
    for j in range(testSize):
        for i in range(dataSize):
           
            total = (data[i].duration-testData[j].duration)**2 +\
                  (data[i].protocol_type - testData[j].protocol_type)**2 +\
                  (data[i].service - testData[j].service)**2 +\
                  (data[i].flag - testData[j].flag)**2 +\
                  (data[i].src_bytes - testData[j].src_bytes)**2 +\
                  (data[i].dst_bytes - testData[j].dst_bytes)**2 +\
                  (data[i].land - testData[j].land)**2 +\
                  (data[i].wrong_fragment - testData[j].wrong_fragment)**2 +\
                  (data[i].urgent - testData[j].urgent)**2 +\
                  (data[i].hot - testData[j].hot)**2 +\
                  (data[i].num_failed_logins - testData[j].num_failed_logins)**2 +\
                  (data[i].logged_in - testData[j].logged_in)**2 +\
                  (data[i].num_compromised - testData[j].num_compromised)**2 +\
                  (data[i].root_shell - testData[j].root_shell)**2 +\
                  (data[i].su_attempted - testData[j].su_attempted) **2 +\
                  (data[i].num_root - testData[j].num_root)**2 +\
                  (data[i].num_file_creations - testData[j].num_file_creations)**2 +\
                  (data[i].num_shells - testData[j].num_shells)** 2 +\
                  (data[i].num_access_files - testData[j].num_access_files)**2 +\
                  (data[i].num_outbound_cmds - testData[j].num_outbound_cmds)**2 +\
                  (data[i].is_host_login - testData[j].is_host_login)**2 +\
                  (data[i].is_guest_login - testData[j].is_guest_login)**2 +\
                  (data[i].count - testData[j].count)**2 +\
                  (data[i].srv_count - testData[j].srv_count)**2 +\
                  (data[i].serror_rate - testData[j].serror_rate)**2 +\
                  (data[i].srv_serror_rate - testData[j].srv_serror_rate)**2 +\
                  (data[i].rerror_rate - testData[j].rerror_rate)**2 +\
                  (data[i].srv_rerror_rate - testData[j].srv_rerror_rate)**2 +\
                  (data[i].same_srv_rate - testData[j].same_srv_rate)**2 +\
                  (data[i].diff_srv_rate - testData[j].diff_srv_rate)**2 +\
                  (data[i].srv_diff_host_rate - testData[j].srv_diff_host_rate)**2 +\
                  (data[i].dst_host_count - testData[j].dst_host_count)**2 +\
                  (data[i].dst_host_srv_count - testData[j].dst_host_srv_count)**2 +\
                  (data[i].dst_host_same_srv_rate - testData[j].dst_host_same_srv_rate)**2 +\
                  (data[i].dst_host_diff_srv_rate - testData[j].dst_host_diff_srv_rate)**2 +\
                  (data[i].dst_host_same_src_port_rate - testData[j].dst_host_same_src_port_rate)**2 +\
                  (data[i].dst_host_srv_diff_host_rate - testData[j].dst_host_srv_diff_host_rate)**2 +\
                  (data[i].dst_host_serror_rate - testData[j].dst_host_serror_rate)**2 +\
                  (data[i].dst_host_srv_serror_rate - testData[j].dst_host_srv_serror_rate)**2 +\
                  (data[i].dst_host_rerror_rate - testData[j].dst_host_rerror_rate)**2 +\
                  (data[i].dst_host_srv_rerror_rate - testData[j].dst_host_srv_rerror_rate)**2

            
            distance.append(total)
            label.append(data[i].label)

        #Set the original values of k-nearest to first k values
        for a in range(int(neighbours)):
            knnMinDistances.append(distance[a])
            knnLabels.append(label[a])

        #Find k-nearest neighbours to jth point
        for i in range(dataSize):
            tempVal = distance[i]
            tempIndex = -1

            for b in range(int(neighbours)):
                if tempVal<knnMinDistances[b]:
                    tempval = knnMinDistances[b]
                    tempIndex = b
                    
            if tempIndex != -1:
                knnMinDistances[tempIndex] = distance[i]
                knnLabels[tempIndex] = label[i]

        #confusion matrix code
        '''max = 0 #amount of guesses the max class has
        maxClass = 0 #class with highest number of guesses

        for a in range(int(neighbours)):
            guess = knnLabels[a]
            knnGuesses[guess]+=1
            if knnGuesses[guess]>max:
                max = knnGuesses[guess]
                maxClass = guess'''
        
        for item in knnLabels:
            fo.write("%s\n" % rLabel[item])

        
        del knnLabels[:]
        del label[:]
        del distance[:]
        del knnMinDistances[:]

    fo.close()

def loadFile():
    """
    loads input and test files
    """
    data = []
    testData = []
    atrributes = []
    tattributes = []
    print "Loading input file....."
    inputFile = open("10percentdata",'r',0)
    for line in inputFile:
        line=line.strip()
        attributes = line.split(",") 
        data.append(networkTraffic(attributes[0],attributes[1],attributes[2],attributes[3],attributes[4],attributes[5],\
                                   attributes[6],attributes[7],attributes[8],attributes[9],attributes[10],attributes[11],\
                                   attributes[12],attributes[13],attributes[14],attributes[15],attributes[16],attributes[17],\
                                   attributes[18],attributes[19],attributes[20],attributes[21],attributes[22],attributes[23],\
                                   attributes[24],attributes[25],attributes[26],attributes[27],attributes[28],attributes[29],\
                                   attributes[30],attributes[31],attributes[32],attributes[33],attributes[34],attributes[35],\
                                   attributes[36],attributes[37],attributes[38],attributes[39],attributes[40],attributes[41]))
    print "number of records inputed="+str(len(data))
    inputFile.close()
    print "Loading test file....."
    testFile = open("10percenttest",'r',0)
    i =0
    for line in testFile:
        i+=1
        line=line.strip()
        tattributes = line.split(",")
        testData.append(networkTraffic(tattributes[0],tattributes[1],tattributes[2],tattributes[3],tattributes[4],tattributes[5],\
                                       tattributes[6],tattributes[7],tattributes[8],tattributes[9],tattributes[10],tattributes[11],\
                                       tattributes[12],tattributes[13],tattributes[14],tattributes[15],tattributes[16],tattributes[17],\
                                       tattributes[18],tattributes[19],tattributes[20],tattributes[21],tattributes[22],tattributes[23],\
                                       tattributes[24],tattributes[25],tattributes[26],tattributes[27],tattributes[28],tattributes[29],\
                                       tattributes[30],tattributes[31],tattributes[32],tattributes[33],tattributes[34],tattributes[35],\
                                       tattributes[36],tattributes[37],tattributes[38],tattributes[39],tattributes[40],"normal."))
    testFile.close()
    print "number of tests inputed="+str(len(testData))
    neighbours = 1
    knn(data, testData, neighbours)

if __name__=="__main__":
    loadFile()
